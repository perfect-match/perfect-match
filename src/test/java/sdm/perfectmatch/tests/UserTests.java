package sdm.perfectmatch.tests;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.text.ParseException;
import java.util.List;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.socialsignin.spring.data.dynamodb.repository.config.EnableDynamoDBRepositories;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.context.PropertyPlaceholderAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.junit4.SpringRunner;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;

import sdm.perfectmatch.entidades.*;
import sdm.perfectmatch.persistencia.*;

/**
 * Classe de testes para a entidade User.
 *  <br>
 * Para rodar, antes sete a seguinte variável de ambiente: -Dspring.config.location=C:/Users/jhcru/sdm/
 *  <br>
 * Neste diretório, criar um arquivo application.properties contendo as seguitnes variáveis:
 * <br>
 * amazon.aws.accesskey=<br>
 * amazon.aws.secretkey=<br>
 * @author jhcru
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {PropertyPlaceholderAutoConfiguration.class, UserTests.DynamoDBConfig.class})
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class UserTests {

    private static Logger LOGGER = LoggerFactory.getLogger(UserTests.class);
    	    
    @Configuration
	@EnableDynamoDBRepositories(basePackageClasses = UserRepository.class)
	public static class DynamoDBConfig {

		@Value("${amazon.aws.accesskey}")
		private String amazonAWSAccessKey;

		@Value("${amazon.aws.secretkey}")
		private String amazonAWSSecretKey;

		public AWSCredentialsProvider amazonAWSCredentialsProvider() {
			return new AWSStaticCredentialsProvider(amazonAWSCredentials());
		}

		@Bean
		public AWSCredentials amazonAWSCredentials() {
			return new BasicAWSCredentials(amazonAWSAccessKey, amazonAWSSecretKey);
		}

		@Bean
		public AmazonDynamoDB amazonDynamoDB() {
			return AmazonDynamoDBClientBuilder.standard().withCredentials(amazonAWSCredentialsProvider())
					.withRegion(Regions.US_EAST_1).build();
		}
	}
    
	@Autowired
	private UserRepository repository;

	@Test
	public void teste1Criacao() throws ParseException {
		LOGGER.info("Criando objetos...");
		User c1 = new User("Gabriel", "Silva", "gabriel@gmail.com", "rua a", "tenho 18 anos e sou trabalhador", "gab", "1", "1", null, false);
		repository.save(c1);

		User c2 = new User("Joao", "Silva", "joao@gmail.com", "rua a", "tenho 18 anos e sou trabalhador", "gab", "2", "2", null, false);
		repository.save(c2);
		
		LOGGER.info("Pesquisado todos");
		Iterable<User> lista = repository.findAll();
		assertNotNull(lista.iterator());
		for (User User : lista) {
			LOGGER.info(User.toString());
		}
		LOGGER.info("Pesquisado um objeto");
		List<User> result = repository.findByFirstName("Gabriel");
		assertEquals(result.size(), 1);
		assertEquals(result.get(0).getEmail(), "gabriel@gmail.com");
		LOGGER.info("Encontrado: {}", result.get(0));
	}
	
	
	@Test
	public void teste2Exclusao() throws ParseException {
		LOGGER.info("Excluindo objetos...");
		
		User c1 = new User("Gabriel", "Silva", "gabriel@gmail.com", "rua a", "tenho 18 anos e sou trabalhador", "gab", "1", "1", null, false);
		repository.delete(c1);
		
		User c2 = new User("Joao", "Silva", "joao@gmail.com", "rua a", "tenho 18 anos e sou trabalhador", "gab", "2", "2", null, false);
		repository.delete(c2);
		List<User> result = repository.findByFirstName("Gabriel");
		
		LOGGER.info("Encontrado: {}", result.size());
		assertEquals(result.size(), 0);
		LOGGER.info("Exclusão feita com sucesso");
	}
}
