package sdm.perfectmatch.negocios;

import java.lang.invoke.MethodHandles;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.commons.collections4.IteratorUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import sdm.perfectmatch.entidades.User;
import sdm.perfectmatch.persistencia.UserRepository;

/**
 * Classe contendo a lógica de negócio para Cotação
 * @author gabrielFernandes-dev
 *
 */

@Service
public class UserService {

    private static final Logger logger= LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    
    private final UserRepository userRepo;

    public UserService(UserRepository userRepository){
        this.userRepo=userRepository;
    }
    
    public List<User> getUsers(){
        if(logger.isInfoEnabled()){
            logger.info("Buscando todos os objetos");
        }
        Iterable<User> lista = this.userRepo.findAll();
        if (lista == null) {
        	return new ArrayList<User>();
        }
        return IteratorUtils.toList(lista.iterator());
    }

    public User getUserByCpf(String cpf){
        if(logger.isInfoEnabled()){
            logger.info("Buscando User com o codigo {}",cpf);
        }
        Optional<User> retorno = this.userRepo.findById(cpf);
        if(!retorno.isPresent()){
            throw new RuntimeException("User com o cpf "+cpf+" nao encontrada");
        }
        return retorno.get();
    }
    
    public List<User> getUserByFirstName(String firstName){
    	if(logger.isInfoEnabled()){
            logger.info("Buscando todos os objetos");
        }
        Iterable<User> lista = this.userRepo.findByCpf(firstName);
        if (lista == null) {
        	return new ArrayList<User>();
        }
        return IteratorUtils.toList(lista.iterator());
    }
    
    public User saveUser(User user){
        if(logger.isInfoEnabled()){
            logger.info("Salvando User com os detalhes {}",user.toString());
        }
        return this.userRepo.save(user);
    }
    
    public void deleteUser(String cpf){
        if(logger.isInfoEnabled()){
            logger.info("Excluindo User com cpf {}",cpf);
        }
        this.userRepo.deleteById(cpf);
    }

    public boolean isUserExists(User user){
    	Optional<User> retorno = this.userRepo.findById(user.getCpf());
        return retorno.isPresent() ? true:  false;
    }
    
    public boolean isUserExists(String cpf){
    	Optional<User> retorno = this.userRepo.findById(cpf);
        return retorno.isPresent() ? true:  false;
    }
}
