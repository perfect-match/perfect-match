package sdm.perfectmatch.persistencia;

import java.util.List;

import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.springframework.data.repository.CrudRepository;

import sdm.perfectmatch.entidades.User;

@EnableScan() 
public interface UserRepository extends CrudRepository<User, String>{
	List<User> findByCpf(String cpf);
	List<User> findByFirstName(String firstName);
}
