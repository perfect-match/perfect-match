package sdm.perfectmatch.rest;

import java.util.List;

import javax.validation.constraints.NotNull;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import sdm.perfectmatch.entidades.User;
import sdm.perfectmatch.negocios.UserService;


@RestController
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, path = "users")
public class UserController {
   
    private final UserService userService;

    public UserController(UserService userService){
        this.userService=userService;
    }

    @GetMapping
    public List<User> getUsers(){
        return userService.getUsers();
    }
    
    @GetMapping(value="{cpf}")
    public User getUserById(@PathVariable String cpf) throws Exception{
        if(!ObjectUtils.isEmpty(cpf)){
           return userService.getUserByCpf(cpf);
        }
        throw new Exception("User com codigo "+cpf+" nao encontrada");
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(value = "", consumes = MediaType.APPLICATION_JSON_VALUE)
    public User createUser(@RequestBody @NotNull User user) throws Exception {
    	if (userService.isUserExists(user.getCpf())) {
    		throw new Exception("User com codigo "+user.getCpf()+" já existe");
    	} 
        return userService.saveUser(user);
    }
    
    @PutMapping(value = "{cpf}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public User updateUser(@PathVariable String cpf, 
    		@RequestBody @NotNull User user) throws Exception {
    	if (!cpf.equals(user.getCpf())) {
    		throw new Exception("Codigo "+cpf+" nao está correto");
    	}
    	if (!userService.isUserExists(user)) {
    		throw new Exception("User com codigo "+cpf+" não existe");
    	}
        return userService.saveUser(user);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping(value = "{cpf}")
    public boolean deleteUser(@PathVariable String cpf) throws Exception {
    	if (!userService.isUserExists(cpf)) {
    		throw new Exception("User com cpf "+cpf+" não existe");
    	} 
    	userService.deleteUser(cpf);
        return true;
    }
    
}
