package sdm.perfectmatch.entidades;

import java.util.Date;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

@DynamoDBTable(tableName = "users")
public class User {
	
	private String firstName, lastName, email, address, bio, nickname, cpf, rg;
	private Date birthDate; 
	private Boolean isPremium;

	public User() {
		super();
	}
	
	
	
	public User(String firstName, String lastName, String email, String address, String bio, String nickname,
			String cpf, String rg, Date birthDate, Boolean isPremium) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.address = address;
		this.bio = bio;
		this.nickname = nickname;
		this.cpf = cpf;
		this.rg = rg;
		this.birthDate = birthDate;
		this.isPremium = isPremium;
	}



	@DynamoDBAttribute
	public String getFirstName() {
		return this.firstName;
	}
	
	@DynamoDBAttribute
	public String getLastName() {
		return lastName;
	}
	@DynamoDBAttribute
	public String getEmail() {
		return email;
	}
	
	@DynamoDBAttribute
	public String getAddress() {
		return address;
	}
	
	@DynamoDBAttribute
	public String getBio() {
		return bio;
	}
	
	@DynamoDBAttribute
	public String getNickname() {
		return nickname;
	}
	
	@DynamoDBHashKey
	public String getCpf() {
		return cpf;
	}
	
	@DynamoDBAttribute
	public String getRg() {
		return rg;
	}
	
	@DynamoDBAttribute
	public Date getBirthDate() {
		return birthDate;
	}
	
	@DynamoDBAttribute
	public Boolean getIsPremium() {
		return isPremium;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setBio(String bio) {
		this.bio = bio;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public void setIsPremium(Boolean isPremium) {
		this.isPremium = isPremium;
	}

	@Override
	public String toString() {
		return "UserAbstract [firstName=" + firstName + ", lastName=" + lastName + ", email=" + email + ", address="
				+ address + ", bio=" + bio + ", nickname=" + nickname + ", cpf=" + cpf + ", rg=" + rg
				+ ", birthDate=" + birthDate + ", isPremium=" + isPremium + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + ((bio == null) ? 0 : bio.hashCode());
		result = prime * result + ((birthDate == null) ? 0 : birthDate.hashCode());
		result = prime * result + ((cpf == null) ? 0 : cpf.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + ((isPremium == null) ? 0 : isPremium.hashCode());
		result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + ((nickname == null) ? 0 : nickname.hashCode());
		result = prime * result + ((rg == null) ? 0 : rg.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof User)) {
			return false;
		}
		User other = (User) obj;
		if (address == null) {
			if (other.address != null) {
				return false;
			}
		} else if (!address.equals(other.address)) {
			return false;
		}
		if (bio == null) {
			if (other.bio != null) {
				return false;
			}
		} else if (!bio.equals(other.bio)) {
			return false;
		}
		if (birthDate == null) {
			if (other.birthDate != null) {
				return false;
			}
		} else if (!birthDate.equals(other.birthDate)) {
			return false;
		}
		if (cpf == null) {
			if (other.cpf != null) {
				return false;
			}
		} else if (!cpf.equals(other.cpf)) {
			return false;
		}
		if (email == null) {
			if (other.email != null) {
				return false;
			}
		} else if (!email.equals(other.email)) {
			return false;
		}
		if (firstName == null) {
			if (other.firstName != null) {
				return false;
			}
		} else if (!firstName.equals(other.firstName)) {
			return false;
		}
		if (isPremium == null) {
			if (other.isPremium != null) {
				return false;
			}
		} else if (!isPremium.equals(other.isPremium)) {
			return false;
		}
		if (lastName == null) {
			if (other.lastName != null) {
				return false;
			}
		} else if (!lastName.equals(other.lastName)) {
			return false;
		}
		if (nickname == null) {
			if (other.nickname != null) {
				return false;
			}
		} else if (!nickname.equals(other.nickname)) {
			return false;
		}
		if (rg == null) {
			if (other.rg != null) {
				return false;
			}
		} else if (!rg.equals(other.rg)) {
			return false;
		}
		return true;
	}

}
