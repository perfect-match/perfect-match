# Perfect Match Cadastro Users

Funções:  
 O cadastro e edição de dados dos usuarios do Perfect Match.  

Foram utilizados(as) neste projeto
 ### Tecnologias: 
 - Java + Spring Framework
 - Docker
 - AWS - ECR, ECS, EC2

 ### Base de Dados
 - AWS - DynamoDB

 ### IDE
 - Eclipse IDE 
 

## Executando o Projeto
 <details><summary>Passos</summary>

 - Baixe a Eclipse [IDE](https://www.eclipse.org/downloads/) ou uma de sua preferência (que contenha o package Manager Maven).
 - Crie uma tabela na base de dados com o nome "users". Saiba [mais](https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/SampleData.CreateTables.html).
 - Crie um arquivo com a extensão .properties com o nome "aplication", nele contendo as chaves de acesso e secreta referente a AWS saiba [como](https://docs.aws.amazon.com/powershell/latest/userguide/pstools-appendix-sign-up.html).
amazon.aws.accesskey={{ chave de acesso }}
amazon.aws.secretkey={{ chave secreta }}
 - Mova este arquivo para dentro do diretório da aplicação.
 - Uma vez que você está dentro da aplicação faça:
Botão direito na Pasta do Projeto > Run As > Maven Install
 este comando fará o download de todas as dependências deste projeto. 
 - Execute o projeto como uma aplicação java.
</details>


